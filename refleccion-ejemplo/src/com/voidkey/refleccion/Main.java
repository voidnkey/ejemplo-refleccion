package com.voidkey.refleccion;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

public class Main {

	public static void main(String[] args) {
		
		Clase1 c1 = new Clase1();
		
		c1.setCantidad(10);
		c1.setDescripcion("test clase 1");
		c1.setNombre("C1");
		
		Clase2 c2 = new Clase2();
		
		c2.setDescripcion("test clase 2");
		c2.setNombre("C2");
		
		
		try {
			refleccionVariables(c2);
			refleccionTest(c2);
			refleccionVariables(c1);
			refleccionTest(c1);
		} catch (Exception e) {
			e.printStackTrace();
		}
		

	}
	
	public static void refleccionVariables(Object objeto) throws Exception
	{
		Field[] fields = objeto.getClass().getFields();
		//Se podran observar las variables depende si es publica.
		for(Field field : fields)
		{
			System.out.println("Variable [["+ field.getName() + "]] Clase [["+ field.getType()+"]]");
		}
	}
	
	
	public static void refleccionTest(Object objeto ) throws Exception
	{
		Method[] metodos = objeto.getClass().getMethods();
		for(Method metodo : metodos)
		{
			// Navegamos x los metodos de la clase para saber que tiene.
			//Si obtenemos como resultado el nombre que buscamos lo ejecutamos para tener el resultado
			if(metodo.getName().equalsIgnoreCase("resultado"))
			{
				System.out.println("Te encuentras en la clase 1");
				String resultado = (String) metodo.invoke(objeto, null);
				System.out.println(resultado);
			}else if(metodo.getName().equalsIgnoreCase("data"))
			{
				System.out.println("Te encuentras en la clase 2");
				String resultado = (String) metodo.invoke(objeto, null);
				System.out.println(resultado);
			}
		}
	}

}
