package com.voidkey.refleccion;

public class Clase1 {
	
	public String nombre;
	public String descripcion;
	public int cantidad;
	
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public int getCantidad() {
		return cantidad;
	}
	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}
	
	public String resultado()
	{
		return this.getNombre() + "-" + this.getDescripcion() + "-"+ this.getCantidad();
	}
}
